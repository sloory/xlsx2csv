package main

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGenerateCSVFromXLSXFile(t *testing.T) {
	var testOutput bytes.Buffer

	testData := []struct {
		input    string
		expected string
	}{
		{"testdata/testfile.xlsx", `Foo;Bar
Baz ;Quuk
`},
		{"testdata/testfile2.xlsx", `Bob;Alice;Sue
Yes;No;Yes
No;;Yes
`},
		{"testdata/emptyCellInRowEnd.xlsx", `Bob;Alice;Sue
Yes;;
No;;Yes
`},
		{"testdata/emptyLinesInFileEnd.xlsx", `Bob;Alice;Sue
Yes;No;Yes
Yes;Yes;Yes
`},
	}

	for _, tc := range testData {
		testOutput.Reset()
		if err := convert(tc.input, &testOutput); err != nil {
			t.Error(err)
		}
		assert.Equal(t, testOutput.String(), tc.expected)
	}
}
