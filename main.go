package main

import (
	"encoding/csv"
	"errors"
	"flag"
	"fmt"
	"github.com/xuri/excelize/v2"
	"io"
	"os"
)

var (
	sheet  int
	output string
	input  string
)

func main() {
	initFlag()

	var outfile *os.File
	var err error
	if outfile, err = os.Create(output); err != nil {
		fmt.Println(err)
	}

	defer func() {
		if closeErr := outfile.Close(); closeErr != nil {
			fmt.Println(err)
		}
	}()

	err = convert(input, outfile)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func initFlag() {
	flag.IntVar(&sheet, "i", 0, "Sheet index")
	flag.StringVar(&output, "o", "", "Output file")

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, `xlsx2csv
	convert the given xlsx file's chosen sheet as a CSV,
Usage:
	xlsx2csv [flags] <xlsx-to-be-converted>
`)
		flag.PrintDefaults()
	}

	flag.Parse()

	if output == "" {
		fmt.Println("Missed output file (-o flag)")
		os.Exit(1)
	}

	if len(flag.Args()) == 0 {
		fmt.Println("Missed input file")
		os.Exit(1)
	}
	input = flag.Args()[0]

}

func convert(input string, output io.Writer) error {

	f, err := excelize.OpenFile(input)
	if err != nil {
		return err
	}
	defer func() {
		if err := f.Close(); err != nil {
			fmt.Println(err)
		}
	}()

	sheetList := f.GetSheetList()
	if sheet >= len(sheetList) {
		return errors.New("wrong sheet index")
	}
	sheetName := f.GetSheetList()[sheet]
	rows, err := f.Rows(sheetName)
	if err != nil {
		return err
	}

	writer := csv.NewWriter(output)
	writer.Comma = ';'
	columnsCount := 0
	for rows.Next() {
		row, err := rows.Columns()
		if err != nil {
			return err
		}
		if columnsCount == 0 {
			columnsCount = len(row)
		} else {
			currColsCount := len(row)
			if currColsCount == 0 {
				continue
			}
			if currColsCount < columnsCount {
				addColumns := columnsCount - currColsCount
				for i := 0; i < addColumns; i++ {
					row = append(row, "")
				}
			}
		}

		err = writer.Write(row)
		if err != nil {
			return err
		}
	}

	if err = rows.Close(); err != nil {
		return err
	}

	writer.Flush()
	if err := writer.Error(); err != nil {
		return err
	}

	return nil
}
