# xlsx2csv
Simple script for converting xlsx files to csv files commandline.
## Usage
```
xlsx2csv
	convert the given xlsx file's chosen sheet as a CSV
Usage:
	xlsx2csv [flags] <xlsx-to-be-converted>
  -i int
    	Sheet index
  -o string
    	Output file
```
